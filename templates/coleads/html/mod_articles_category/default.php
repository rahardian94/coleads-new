<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_category
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
<style>
	.category-module {
		list-style: none;
	}
	a {
		color: gray;
	}
	a:hover {
		color: #b9a382;
	}
	.mod-articles-category-introtext {
		font-size: 14px;
		line-height: 20px;
	}
	.mod-articles-category-title {
		font-weight: 500;
		font-size: 18px;
		line-height: 20px;
	}
	li {
		margin-bottom: 50px;
	}

	body {
	font-size: 14px;
	line-height: 20px;
	}

	h1, h2, h3, h4, h5, h6 {
	font-family: 'Poppins';
	}

	h1 {
	font-size: 16px;
	margin-bottom: .5em;
	line-height: 20px;
	}

	p {
	font-family: 'Poppins';
	font-size: .8rem;
	}

	.authoritative {
		background-color: #A9ACBC;
		text-align: left;
		display: inline-block;
		font-family: 'Poppins';
		padding: 4px 7px 4px 7px;
		color: white;
		font-size: 12px;
		font-weight: 500;
		margin-bottom: 30px;
	}

	.container {
	max-width: 1024px;
	width: 90%;
	margin: 0 auto;
	}

	.timeline-item {
	padding: 2em 2em 2em;
	position: relative;
	color: rgba(0, 0, 0, 0.7);
	border-left: 2px solid rgba(0, 0, 0, 0.3);
	}
	.timeline-item p {
	font-size: 14px;
	}
	.timeline-item::before {
	content: attr(date-is);
	position: absolute;
	left: 2em;
	font-weight: bold;
	top: -4px;
	display: block;
	font-family: 'Poppins';
	font-weight: 700;
	font-size: .785rem;
	}
	.timeline-item::after {
	width: 10px;
	height: 10px;
	display: block;
	/* color: #b9a382; */
	top: 0;
	position: absolute;
	left: -7px;
	border-radius: 10px;
	content: '';
	/* border: 2px solid rgba(0, 0, 0, 0.3); */
	background: url('./images/2019/03/11/bullet.jpg');
	}
	.timeline-item:last-child {
	-o-border-image: linear-gradient(to bottom, rgba(0, 0, 0, 0.3) 60%, transparent) 1 100%;
	border-image: -webkit-linear-gradient(top, rgba(0, 0, 0, 0.3) 60%, transparent) 1 100%;
	border-image: linear-gradient(to bottom, rgba(0, 0, 0, 0.3) 60%, transparent) 1 100%;
	}
</style>

<ul class="category-module<?php echo $moduleclass_sfx; ?> mod-list" >
	<?php if ($grouped) : ?>
		<?php foreach ($list as $group_name => $group) : ?>
		<li>
			<div class="mod-articles-category-group"></div>
			
				<?php foreach ($group as $item) : ?>
				<div class="timeline-item" date-is="<?php echo $item->displayDate ?>">
				
						<?php if ($params->get('link_titles') == 1) : ?>
						<h1><a href="<?php echo $item->link; ?>">
								<?php echo $item->title; ?>
							</a></h1>
						<?php else : ?>
						<h1><?php echo $item->title; ?></h1>
						<?php endif; ?>

						<?php if ($item->displayHits) : ?>
							<span class="mod-articles-category-hits">
								(<?php echo $item->displayHits; ?>)
							</span>
						<?php endif; ?>

						<?php if ($params->get('show_author')) : ?>
							<span class="mod-articles-category-writtenby">
								<?php echo $item->displayAuthorName; ?>
							</span>
						<?php endif; ?>

						<?php if ($item->displayCategoryTitle) : ?>
							<span class="mod-articles-category-category">
								(<?php echo $item->displayCategoryTitle; ?>)
							</span>
						<?php endif; ?>

						<?php if ($params->get('show_tags', 0) && $item->tags->itemTags) : ?>
							<div class="mod-articles-category-tags">
								<?php echo JLayoutHelper::render('joomla.content.tags', $item->tags->itemTags); ?>
							</div>
						<?php endif; ?>

						<?php if ($params->get('show_introtext')) : ?>
							<p>
								<?php echo $item->displayIntrotext; ?>
							</p>
						<?php endif; ?>

						<?php if ($params->get('show_readmore')) : ?>
							<p class="mod-articles-category-readmore">
								<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>">
									<?php if ($item->params->get('access-view') == false) : ?>
										<?php echo JText::_('MOD_ARTICLES_CATEGORY_REGISTER_TO_READ_MORE'); ?>
									<?php elseif ($readmore = $item->alternative_readmore) : ?>
										<?php echo $readmore; ?>
										<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
											<?php if ($params->get('show_readmore_title', 0) != 0) : ?>
												<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
											<?php endif; ?>
									<?php elseif ($params->get('show_readmore_title', 0) == 0) : ?>
										<?php echo JText::sprintf('MOD_ARTICLES_CATEGORY_READ_MORE_TITLE'); ?>
									<?php else : ?>
										<?php echo JText::_('MOD_ARTICLES_CATEGORY_READ_MORE'); ?>
										<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
									<?php endif; ?>
								</a>
							</p>
						<?php endif; ?>
						</div>
				<?php endforeach; ?>
		</li>
		<?php endforeach; ?>
	<?php else : ?>
		<div class="authoritative">NEWS FROM AUTHORITATIVE SITES</div>
		 <?php foreach ($list as $item) : ?>
			<li>
				<?php if ($params->get('link_titles') == 1) : ?>
					<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
				<?php else : ?>
					<?php echo $item->title; ?>
				<?php endif; ?>

				<?php if ($item->displayHits) : ?>
					<span class="mod-articles-category-hits">
						(<?php echo $item->displayHits; ?>)
					</span>
				<?php endif; ?>

				<?php if ($params->get('show_author')) : ?>
					<span class="mod-articles-category-writtenby">
						<?php echo $item->displayAuthorName; ?>
					</span>
				<?php endif; ?>

				<?php if ($item->displayCategoryTitle) : ?>
					<span class="mod-articles-category-category">
						(<?php echo $item->displayCategoryTitle; ?>)
					</span>
				<?php endif; ?>

				<?php if ($item->displayDate) : ?>
					<span class="mod-articles-category-date">
						<?php echo $item->displayDate; ?>
					</span>
				<?php endif; ?>

				<?php if ($params->get('show_tags', 0) && $item->tags->itemTags) : ?>
					<div class="mod-articles-category-tags">
						<?php echo JLayoutHelper::render('joomla.content.tags', $item->tags->itemTags); ?>
					</div>
				<?php endif; ?>

				<?php if ($params->get('show_introtext')) : ?>
					<p class="mod-articles-category-introtext">
						<?php echo $item->displayIntrotext; ?>
					</p>
				<?php endif; ?>

				<?php if ($params->get('show_readmore')) : ?>
					<p class="mod-articles-category-readmore">
						<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>">
							<?php if ($item->params->get('access-view') == false) : ?>
								<?php echo JText::_('MOD_ARTICLES_CATEGORY_REGISTER_TO_READ_MORE'); ?>
							<?php elseif ($readmore = $item->alternative_readmore) : ?>
								<?php echo $readmore; ?>
								<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
							<?php elseif ($params->get('show_readmore_title', 0) == 0) : ?>
								<?php echo JText::sprintf('MOD_ARTICLES_CATEGORY_READ_MORE_TITLE'); ?>
							<?php else : ?>
								<?php echo JText::_('MOD_ARTICLES_CATEGORY_READ_MORE'); ?>
								<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
							<?php endif; ?>
						</a>
					</p>
				<?php endif; ?>
			</li>
		<?php endforeach; ?>
	<?php endif; ?>
</ul>
